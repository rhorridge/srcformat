RM := rm

all: srcformat.sty srcformat.pdf

%.sty: %.ins %.dtx
	yes | latex $<

%.pdf: %.dtx
	pdflatex $<

clean:
	$(RM) -f *.aux *.fdb_latexmk *.fls *.log *.sty *.pdf *.src *.bbl *.opts *.srcs
